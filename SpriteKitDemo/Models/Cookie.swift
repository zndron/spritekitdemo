//
//  Cookie.swift
//  SpriteKitDemo
//
//  Created by Andrii Bondarchuk on 11/6/17.
//  Copyright © 2017 Andrii Bondarchuk. All rights reserved.
//

import SpriteKit

enum CookieType: Int, CustomStringConvertible {
    case unknown = 0, color1, color2, color3, color4, color5, color6
    
    var spriteName: String {
        let spriteNames = [
            "Color1",
            "Color2",
            "Color3",
            "Color4",
            "Color5",
            "Color6"
        ]
        
        return spriteNames[rawValue - 1]
    }

    var description: String {
        return spriteName
    }
    
    static func random() -> CookieType {
        return CookieType(rawValue: Int(arc4random_uniform(6)) + 1)!
    }
}

class Cookie: CustomStringConvertible, Hashable {
    
    var column: Int
    var row: Int
    let cookieType: CookieType
    var sprite: SKSpriteNode?
    
    var description: String {
        return "type:\(cookieType) square:(\(column),\(row))"
    }
    
    var hashValue: Int {
        return row*10 + column
    }
    
    init(column: Int, row: Int, cookieType: CookieType) {
        self.column = column
        self.row = row
        self.cookieType = cookieType
    }
    
    static func == (lhs: Cookie, rhs: Cookie) -> Bool {
        return lhs.column == rhs.column && lhs.row == rhs.row
    }
}
