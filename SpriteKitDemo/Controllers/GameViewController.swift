//
//  GameViewController.swift
//  SpriteKitDemo
//
//  Created by Andrii Bondarchuk on 11/5/17.
//  Copyright © 2017 Andrii Bondarchuk. All rights reserved.
//

import UIKit
import SpriteKit

class GameViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Configure the view.
       // let scene = GameScene(size: self.view.bounds.size)
        
        let scene = MenuScene(size: self.view.bounds.size)
        
        let skView = self.view as! SKView
        
        skView.allowsTransparency = true
        skView.ignoresSiblingOrder = true
        
        skView.showsFPS = true
        skView.showsNodeCount = true
        
        // Set the scale mode to scale to fit the window.
        scene.scaleMode = .aspectFit
        
        skView.presentScene(scene)
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return [.portrait, .portraitUpsideDown]
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
}
