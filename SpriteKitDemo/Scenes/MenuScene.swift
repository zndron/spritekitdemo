//
//  MenuScene.swift
//  SpriteKitDemo
//
//  Created by Andrii Bondarchuk on 11/13/17.
//  Copyright © 2017 Andrii Bondarchuk. All rights reserved.
//

import SpriteKit

class MenuScene: SKScene {
    
    var titleLabel: SKLabelNode!
    var playButton: SKSpriteNode!

    override func didMove(to view: SKView) {
        setupView()
        setupTitle()
        setupPlayButton()
    }
    
    func setupView() {
        backgroundColor = UIColor(red:0.32, green:0.27, blue:0.42, alpha:1.0)

        scaleMode = .aspectFill
    }
    
    func setupTitle() {
        titleLabel = SKLabelNode(text: "Color Match Jam")
        
        titleLabel.fontSize = 40
        titleLabel.fontColor = .white
        titleLabel.fontName = "Helvetica"
        titleLabel.position = CGPoint(x: size.width/2, y: size.height/1.33)
        
        addChild(titleLabel)
    }
    
    func setupPlayButton() {
        playButton = SKSpriteNode(imageNamed: "play-btn")
        
        playButton.position = CGPoint(x: size.width/2, y: size.height/2)
        playButton.name = "Play"
        
        addChild(playButton)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        let touchLocation = touch!.location(in: self)
        
        if playButton.contains(touchLocation) {
            let scene = GameScene(size: self.size)
            self.view?.presentScene(scene)
        }
    }
}
