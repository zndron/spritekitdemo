//
//  GameOverNode.swift
//  SpriteKitDemo
//
//  Created by Andrii Bondarchuk on 11/14/17.
//  Copyright © 2017 Andrii Bondarchuk. All rights reserved.
//

import SpriteKit

class GameOverNode: SKNode {

    var titleLabel: SKLabelNode!
    var scoreLabel: SKLabelNode!
    var replayButton: SKSpriteNode!
    var quitButton: SKSpriteNode!
    
    var backgroundNode: SKSpriteNode = SKSpriteNode()
    
    // Init
    static func create(_ size: CGSize) -> GameOverNode {
        let node = GameOverNode()
        node.setupView(size)
        return node
    }
    
    func setupView(_ size: CGSize) {
        
        backgroundNode.size = size
        backgroundNode.color = UIColor(red:0.32, green:0.27, blue:0.42, alpha:1.0)
        backgroundNode.colorBlendFactor = 1
        backgroundNode.isUserInteractionEnabled = true
        addChild(backgroundNode)
        
        // titleLabel
        titleLabel = SKLabelNode(text: "Game Over")
        
        titleLabel.fontSize = 40
        titleLabel.fontColor = .white
        titleLabel.fontName = "Helvetica"
        titleLabel.position = CGPoint(x: 0, y: size.height/4)
        
        addChild(titleLabel)

        // replayButton
        replayButton = SKSpriteNode(imageNamed: "replay-btn")
        
        replayButton.position = CGPoint(x: 0, y: 0)
        replayButton.name = "Replay"
        
        addChild(replayButton)
        
        // quitButton
        quitButton = SKSpriteNode(imageNamed: "quit-btn")
        
        quitButton.position = CGPoint(x: 0, y: -replayButton.size.height*1.5)
        quitButton.name = "Quit"
        
        addChild(quitButton)
    }
    
    func setScore(_ score: Int) {
        scoreLabel = SKLabelNode(text: "\(score)")
        
        scoreLabel.fontColor = .white
        scoreLabel.fontSize = 40
        scoreLabel.fontName = "Helvetica"
        scoreLabel.position = CGPoint(x: 0, y: replayButton.size.height*1.5)
        addChild(scoreLabel)
    }
    
    func show() {
        zPosition = 999
        isHidden = false
    }
    
    func hide() {
        zPosition = -999
        isHidden = true
    }
}
