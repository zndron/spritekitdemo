//
//  GameScene.swift
//  SpriteKitDemo
//
//  Created by Andrii Bondarchuk on 11/5/17.
//  Copyright © 2017 Andrii Bondarchuk. All rights reserved.
//

import SpriteKit

class GameScene: SKScene {

    // MARK: Properties
    
    var score = 0
    
    var seconds = 30
    var timer = Timer()
    
    var level: Level!
    
    let scoreNode = SKLabelNode()
    let timerNode = SKLabelNode()

    var pauseButton: SKSpriteNode!
    
    var gameOverNode: GameOverNode!
    var pauseNode: GameOverNode!
    
    let TileWidth: CGFloat = 34.0
    let TileHeight: CGFloat = 34.0
    
    let gameLayer = SKNode()
    let cookiesLayer = SKNode()
    
    private var swipeFromColumn: Int?
    private var swipeFromRow: Int?
    
    var swipeHandler: ((Swap) -> ())?
    
    override func didMove(to view: SKView) {
        setupView()
        setupScore()
        setupTimer()
        setupPauseButton()
    }

    func setupView() {
        backgroundColor = UIColor(red:0.32, green:0.27, blue:0.42, alpha:1.0)
        
        scaleMode = .aspectFill
        
        anchorPoint = CGPoint(x: 0.5, y: 0.5)
        
        addChild(gameLayer)
        
        let layerPosition = CGPoint(
            x: -TileWidth * CGFloat(NumColumns) / 2,
            y: -TileHeight * CGFloat(NumRows) / 2
        )

        cookiesLayer.position = layerPosition
        gameLayer.addChild(cookiesLayer)
        
        gameOverNode = GameOverNode.create(size)
        gameOverNode.position = CGPoint(x: 0, y: 0)
        gameOverNode.hide()
  
        gameLayer.addChild(gameOverNode)
        
//        gameOverNode.show()
//        gameOverNode.setScore(1000)
        
        swipeFromColumn = nil
        swipeFromRow = nil
        
        level = Level()
        swipeHandler = handleSwipe(_:)

        shuffle()
    }
    
    func setupScore() {
        
        scoreNode.text = "\(score)"
        scoreNode.fontSize = 34
        scoreNode.fontColor = SKColor.white
        scoreNode.fontName = "Helvetica"
        scoreNode.position = CGPoint(x: 0, y: size.height / 2 - 50)
        
        gameLayer.addChild(scoreNode)
    }
    
    func reloadScore() {
        scoreNode.text = "\(score)"
    }
    
    func setupTimer() {
        
        timerNode.text = "\(seconds)"
        timerNode.fontSize = 34
        timerNode.fontColor = SKColor.white
        timerNode.fontName = "Helvetica"
        timerNode.position = CGPoint(x: -100, y: size.height / 2 - 50)
        
        gameLayer.addChild(timerNode)
        
        runTimer()
    }

    func runTimer() {
        timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) {
            [weak self] timer in self?.updateTimer()
        }
    }
    
    func updateTimer() {
        if seconds < 1 {
            timer.invalidate()
            gameOverNode.show()
            gameOverNode.setScore(score)
        } else {
            seconds -= 1
            timerNode.text = "\(seconds)"
        }
    }

    func setupPauseButton() {
        pauseButton = SKSpriteNode(imageNamed: "pause-symbol")
        pauseButton.position = CGPoint(x: 100, y: size.height / 2 - 50 + 14)
        pauseButton.name = "Pause"
        pauseButton.scale(to: CGSize(width: 26, height: 26))
        
        addChild(pauseButton)
    }
    
    func shuffle() {
        let newCookies = level.shuffle()
        addSprites(for: newCookies)
    }
    
    // Swipe handler.
    func handleSwipe(_ swap: Swap) {
        self.view!.isUserInteractionEnabled = false
        
        if level.isPossibleSwap(swap) {
            // stop timer
            timer.invalidate()
            
            level.performSwap(swap)
            animate(swap, completion: handleMatches)
            
            // run timer
            runTimer()
        } else {
            animateInvalidSwap(swap) {
                self.view!.isUserInteractionEnabled = true
            }
        }
    }
    
    func beginNextTurn() {
        level.detectPossibleSwaps()
        self.view!.isUserInteractionEnabled = true
    }
    
    func handleMatches() {
        let chains = level.removeMatches()
        
        if chains.count == 0 {
            beginNextTurn()
            return
        }
        
        animateMatchedCookies(for: chains) {
            
            // Add the new scores to the total.
            for chain in chains {
                self.score += chain.score
                self.seconds += chain.seconds
            }
            
            self.reloadScore()
            
            let columns = self.level.fillHoles()
            
            self.animateFallingCookies(columns) {
                
                let columns = self.level.topUpCookies()
                
                self.animateNewCookies(columns) {
                    self.handleMatches()
                }
            }
        }
    }
    
    
    // MARK: Level Setup
    
    func addSprites(for cookies: Set<Cookie>) {
        for cookie in cookies {
            // Create a new sprite for the cookie and add it to the cookiesLayer.
            let sprite = SKSpriteNode(imageNamed: cookie.cookieType.spriteName)
            
            sprite.size = CGSize(width: TileWidth, height: TileHeight)
            sprite.position = pointFor(column: cookie.column, row: cookie.row)
            cookiesLayer.addChild(sprite)
            
            cookie.sprite = sprite
        }
    }
    
    // MARK: Point conversion
    
    // Converts a column,row pair into a CGPoint that is relative to the cookieLayer.
    func pointFor(column: Int, row: Int) -> CGPoint {
        return CGPoint(
            x: CGFloat(column)*TileWidth + TileWidth/2,
            y: CGFloat(row)*TileHeight + TileHeight/2
        )
    }
    
    // Converts a point relative to the cookieLayer into column and row numbers.
    func convertPoint(_ point: CGPoint) -> (success: Bool, column: Int, row: Int) {
        // Is this a valid location within the cookies layer? If yes,
        // calculate the corresponding row and column numbers.
        if point.x >= 0 && point.x < CGFloat(NumColumns)*TileWidth &&
            point.y >= 0 && point.y < CGFloat(NumRows)*TileHeight
        {
            return (true, Int(point.x / TileWidth), Int(point.y / TileHeight))
        } else {
            return (false, 0, 0)  // invalid location
        }
    }
    
    // MARK: Cookie Swapping
    
    // We get here after the user performs a swipe. This sets in motion a whole
    // chain of events: 1) swap the cookies, 2) remove the matching lines, 3)
    // drop new cookies into the screen, 4) check if they create new matches,
    // and so on.
    func trySwap(horizontal horzDelta: Int, vertical vertDelta: Int) {
        let toColumn = swipeFromColumn! + horzDelta
        let toRow = swipeFromRow! + vertDelta
        
        // Going outside the bounds of the array? This happens when the user swipes
        // over the edge of the grid. We should ignore such swipes.
        guard toColumn >= 0 && toColumn < NumColumns else { return }
        guard toRow >= 0 && toRow < NumRows else { return }
        
        // Can't swap if there is no cookie to swap with. This happens when the user
        // swipes into a gap where there is no tile.
        if  let toCookie = level.cookieAt(column: toColumn, row: toRow),
            let fromCookie = level.cookieAt(column: swipeFromColumn!, row: swipeFromRow!),
            let handler = swipeHandler {
            // Communicate this swap request back to the ViewController.
            let swap = Swap(cookieA: fromCookie, cookieB: toCookie)
            handler(swap)
        }
    }
    
    // MARK: Animations
    
    func animate(_ swap: Swap, completion: @escaping () -> ()) {
        let spriteA = swap.cookieA.sprite!
        let spriteB = swap.cookieB.sprite!
        
        spriteA.zPosition = 100
        spriteB.zPosition = 90
        
        let duration: TimeInterval = 0.3
        
        let moveA = SKAction.move(to: spriteB.position, duration: duration)
        moveA.timingMode = .easeOut
        spriteA.run(moveA, completion: completion)
        
        let moveB = SKAction.move(to: spriteA.position, duration: duration)
        moveB.timingMode = .easeOut
        spriteB.run(moveB)
    }
    
    func animateInvalidSwap(_ swap: Swap, completion: @escaping () -> ()) {
        let spriteA = swap.cookieA.sprite!
        let spriteB = swap.cookieB.sprite!
        
        spriteA.zPosition = 100
        spriteB.zPosition = 90
        
        let duration: TimeInterval = 0.2
        
        let moveA = SKAction.move(to: spriteB.position, duration: duration)
        moveA.timingMode = .easeOut
        
        let moveB = SKAction.move(to: spriteA.position, duration: duration)
        moveB.timingMode = .easeOut
        
        spriteA.run(SKAction.sequence([moveA, moveB]), completion: completion)
        spriteB.run(SKAction.sequence([moveB, moveA]), completion: completion)
    }
    
    func animateMatchedCookies(for chains: Set<Chain>, completion: @escaping () -> ()) {
        for chain in chains {
            for cookie in chain.cookies {
                if let sprite = cookie.sprite {
                    if sprite.action(forKey: "removing") == nil {
                        let scaleAction = SKAction.scale(to: 0.1, duration: 0.3)
                        scaleAction.timingMode = .easeOut
                        sprite.run(SKAction.sequence([scaleAction, SKAction.removeFromParent()]), withKey:"removing")
                    }
                }
            }
        }
   
        run(SKAction.wait(forDuration: 0.3), completion: completion)
    }
    
    func animateFallingCookies(_ columns: [[Cookie]], completion: @escaping () -> ()) {
        var longestDuration: TimeInterval = 0
        for array in columns {
            for (idx, cookie) in array.enumerated() {
                let newPosition = pointFor(column: cookie.column, row: cookie.row)
               
                let delay = 0.05 + 0.15*TimeInterval(idx)
              
                let sprite = cookie.sprite!   // sprite always exists at this point
                let duration = TimeInterval(((sprite.position.y - newPosition.y) / TileHeight) * 0.1)
               
                longestDuration = max(longestDuration, duration + delay)
              
                let moveAction = SKAction.move(to: newPosition, duration: duration)
                moveAction.timingMode = .easeOut
                sprite.run(
                    SKAction.sequence([
                        SKAction.wait(forDuration: delay),
                        moveAction
                    ])
                )
            }
        }
        
        run(SKAction.wait(forDuration: longestDuration), completion: completion)
    }
    
    func animateNewCookies(_ columns: [[Cookie]], completion: @escaping () -> ()) {
        var longestDuration: TimeInterval = 0
        
        for array in columns {
            let startRow = array[0].row + 1
            
            for (idx, cookie) in array.enumerated() {
             
                let sprite = SKSpriteNode(imageNamed: cookie.cookieType.spriteName)
                sprite.size = CGSize(width: TileWidth, height: TileHeight)
                sprite.position = pointFor(column: cookie.column, row: startRow)
                cookiesLayer.addChild(sprite)
                cookie.sprite = sprite
              
                let delay = 0.1 + 0.2 * TimeInterval(array.count - idx - 1)
             
                let duration = TimeInterval(startRow - cookie.row) * 0.1
                longestDuration = max(longestDuration, duration + delay)
              
                let newPosition = pointFor(column: cookie.column, row: cookie.row)
                let moveAction = SKAction.move(to: newPosition, duration: duration)
                moveAction.timingMode = .easeOut
                sprite.alpha = 0
                sprite.run(
                    SKAction.sequence([
                        SKAction.wait(forDuration: delay),
                        SKAction.group([
                            SKAction.fadeIn(withDuration: 0.05),
                            moveAction
                        ])
                    ])
                )
            }
        }
     
        run(SKAction.wait(forDuration: longestDuration), completion: completion)
    }
    
    // MARK: Cookie Swipe Handlers
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        let cookiesLocation = touch!.location(in: cookiesLayer)

        // If the touch is inside a square, then this might be the start of a swipe motion.
        let (success, column, row) = convertPoint(cookiesLocation)
        
        if success {
            // The touch must be on a cookie, not on an empty tile.
            if level.cookieAt(column: column, row: row) != nil {
                // Remember in which column and row the swipe started, so we can compare
                // them later to find the direction of the swipe. This is also the first
                // cookie that will be swapped.
                swipeFromColumn = column
                swipeFromRow = row
            }
        }
        
        for touch in touches {
            let nodeAtTouch = self.gameOverNode?.atPoint(touch.location(in: self.gameOverNode))
            
            if nodeAtTouch!.name == "Replay" {
                print("Replay Touched")
                
//                score = 0
//                seconds = 30
//
//                scoreNode.text = "\(score)"
//                timerNode.text = "\(seconds)"
//
//                runTimer()
//
//                self.gameOverNode?.removeFromParent()
            }
            if nodeAtTouch!.name == "Quit" {
                print("Quit Touched")
//                self.gameOverNode?.removeFromParent()
            }
        }
        
        let touchLocation = touch!.location(in: self)
        
        if pauseButton.contains(touchLocation) {
            print("Pause Touched")
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        // If swipeFromColumn is nil then either the swipe began outside
        // the valid area or the game has already swapped the cookies and we need
        // to ignore the rest of the motion.
        guard swipeFromColumn != nil else { return }
        
        guard let touch = touches.first else { return }
        let location = touch.location(in: cookiesLayer)
        
        let (success, column, row) = convertPoint(location)
        
        if success {
            // Figure out in which direction the player swiped. Diagonal swipes
            // are not allowed.
            var horzDelta = 0, vertDelta = 0
            
            if column < swipeFromColumn! {          // swipe left
                horzDelta = -1
            } else if column > swipeFromColumn! {   // swipe right
                horzDelta = 1
            } else if row < swipeFromRow! {         // swipe down
                vertDelta = -1
            } else if row > swipeFromRow! {         // swipe up
                vertDelta = 1
            }
            
            // Only try swapping when the user swiped into a new square.
            if horzDelta != 0 || vertDelta != 0 {
                trySwap(horizontal: horzDelta, vertical: vertDelta)
                
                // Ignore the rest of this swipe motion from now on.
                swipeFromColumn = nil
            }
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        // If the gesture ended, regardless of whether if was a valid swipe or not,
        // reset the starting column and row numbers.
        swipeFromColumn = nil
        swipeFromRow = nil
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        touchesEnded(touches, with: event)
    }
}
